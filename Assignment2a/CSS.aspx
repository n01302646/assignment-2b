﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="CSS.aspx.cs" Inherits="Assignment2a.CSS" %>

<asp:Content ContentPlaceHolderID="Mainpage" runat="server"> 
    <div class="jumbotron">
        <h1>CSS</h1>
        <p class="lead">Welcome to study guide of CSS</p>
        </div>
    </asp:Content>
<asp:Content ContentPlaceHolderID="Intro" runat="server"> 
    <h3>CSS Intro</h3>
        CSS is a language that describes the style of an HTML document.CSS describes how HTML elements should be displayed.
</asp:Content>
<asp:Content ContentPlaceHolderID="OwnExample" runat="server"> 
   
    <h3>My example</h3>
     
     <Asp:CodeBox ID="My_FrontEnd_Code" runat="server"
        SkinId="CodeBox" code="CSSOwnExample" owner="Me">
     </Asp:CodeBox>
  
</asp:Content>         
<asp:Content ContentPlaceHolderID="FoundExample" runat="server">     
        <h3>Example from web</h3>
    <Asp:CodeBox ID="CodeBox1" runat="server"
        SkinId="CodeBox" code="CSSFoundExample" owner="Teacher">
     </Asp:CodeBox>      
</asp:Content>     
<asp:Content ContentPlaceHolderID="Links" runat="server">       
    <h3>Links</h3>
  <a href="#">Elements</a>
  <a href="#">Border</a>
  <a href="#">Margin</a>
  <a href="#">Padding</a>
 </asp:Content>
     