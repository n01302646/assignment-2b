﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assignment2a._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="Mainpage" runat="server">

    <div class="jumbotron">
        <h1>STUDY GUIDE</h1>
        <p class="lead">Welcome to study guide of javascript, CSS and SQL</p>
        </div>
    </asp:Content>
    <asp:Content ID="Content1" ContentPlaceHolderID="Intro" runat="server">
            <h2>Learn tricks</h2>
                <p>
                   see overview explaining a tricky concept in javascript, CSS and SQL
                </p>                
      </asp:Content>
 <asp:Content ID="Content3" ContentPlaceHolderID="OwnExample" runat="server">
   
        <%-- <div class="col-md-6">--%>
                <h2>Own Examples</h2>
                <p>
                    Get an example of a snippet of code i wrote myself.
                </p>
     <br /><br />
                <h3>Little History of web</h3>
                    <p>Strongly based on the Standard Generalized Markup Language (SGML),<br />
                        HTML became the fundamental building block of the World Wide Web,<br />
                        and remains at the core of its coding and infrastructure. <br />
                        The standard enabled coders with the ability to organize web<br />
                        page layouts that could be understood and interacted with over<br />
                        interconnected networks.</p>
                
           
     </asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FoundExample" runat="server">
 
                 <h2>Found Example</h2>
                <p>
                    Get an example of a snippet of code i found online.
                </p> 
                <br /><br />
                <h3>Little about Author</h3>
                <H4> JOBANJIT SINGH</H4>
	            <h5>Bio-Web Developer</h5>
	            <p>Hi there! I am 23 year B.Tech Graduate in Computer Sciences and have many certification in same field. 10+ of my coded websites are already up and ruuning. </p>
	            <p>	I Love to design websites with responsive design and interestng interfaces.If you want to convert potential visitor to customers, Get in touch </br></br> M: +1 6474-705-159  |  Mail:Jobanjitpannu@outlook.com </p>
	
            <%--</div>--%>
</asp:Content>
              <asp:Content ID="Content2" ContentPlaceHolderID="Links" runat="server">
    
       
                <h2>Get links to concepts</h2>
                <p>
                  Get links to concepts explaining the topics
                </p>
                
            </asp:Content>
        

