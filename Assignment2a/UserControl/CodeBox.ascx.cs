﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Assignment2a
{
    public partial class CodeBox : System.Web.UI.UserControl
    {
        public string code
        {
            get { return (string)ViewState["code"]; }
            set { ViewState["code"] = value; }

        }

        public string owner
        {
            get { return (string)ViewState["owner"]; }
            set { ViewState["owner"] = value; }

        }









        DataView CreateCodeSource()
        {
            DataTable codedata = new DataTable();


            //First column is the line number of the code (idx -> index)
            DataColumn idx_col = new DataColumn();

            //SEcond column is the actual code itself
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

            //These Tildas will be replaced with line indents
            //When the code lines are bound to rows.
            //If you have double quotes in your html code, escap them with \
            //eg. <div style=\"background:green;\"></div>   
            List<string> back_end_code;
            if (code== "CSSOwnExample" && owner == "Me")
            {
            back_end_code = new List<string>(new string[]{
            "body",
            "~  {",
            "~~background-color: lightblue;",
            "~}",
            "h1",
            "~{",
            "~~color: white;",
            "~~text-align: center;",
            "~}",
             "p {",
             "~~font-family: verdana;",
              "~~font-size: 20px;",
               "}",
               });

            }
            else if (code == "CSSFoundExample" && owner == "Teacher")
            {
                back_end_code = new List<string>(new string[]{
            "<style>",
            "body {",
            "~background-image: url("+"img_tree.png"+");",
            "~background - repeat: no - repeat;",
            "~background - position: right top;",
            "~margin - right: 200px;",
            "~< h1 style = background-color:#000000; >#000000</h1>",
            "~< h1 style = background-color:#3c3c3c; >#3c3c3c</h1>",
            "~< h1 style = background-color:#787878; >#787878</h1>",
            "~< h1 style = background-color:#b4b4b4; >#b4b4b4</h1>",
            "}",
            "</ style >",
            });
            }

            else if (code == "JSOwnExample" && owner == "Me")
            {
                back_end_code = new List<string>(new string[]{
          "function createTable(rows, cols)",
          "{",
          "~var j=1;",
          "~var output = <table border='1' width='500'>;",
           "~for (i = 1; i <= rows; i++){",
            "~~output = output + <tr>;",
             "~~while (j <= cols) {",
             "~~~output = output + <td> + i * j + </td>;",
             "~~~ j = j + 1;",
              "~~}",
               "~~ output = output + </tr>;",
               "~j = 1;",
                "}",
            });
            }
            else if (code == "JSFoundExample" && owner == "Teacher")
            {
                back_end_code = new List<string>(new string[]{
           "<script type="+"text/javascript"+">",
           "~var rows = prompt("+"No. of rows?"+");",
             "~var cols = prompt("+"No. of columns"+");",
              "~if (rows == "+" || rows == null)",
                    "~~rows = 10;",
               "~ if (cols == "+" || cols == null)",
                    "~~cols = 10;",
               "~createTable(rows, cols);",
                    "</script>"
            });
            }
            else if (code == "ASPOwnExample" && owner == "Me")
            {
                back_end_code = new List<string>(new string[]{
            "~String.Format("+"{0:y yy yyy yyyy}"+", dt);  // 8 08 008 2008", 
                "~String.Format(" + "{ 0:M MM MMM MMMM}" + ", dt);  // 3 03 Mar March",
                "~String.Format(" + "{ 0:d dd ddd dddd}" + ", dt);  // 9 09 Sun Sunday",
                "~String.Format(" + "{ 0:h hh H HH}" + ",     dt);  // 4 04 16 16",
                "~String.Format(" + "{ 0:s ss}" + ",          dt);  // 7 07",         
                "~String.Format(" + "{ 0:f ff fff ffff}" + ", dt);  // 1 12 123 1230"  , 
                "~String.Format(" + "{ 0:F FF FFF FFFF}" + ", dt);  // 1 12 123 123" ,   
                "~String.Format(" + "{ 0:t tt}" + ",          dt);  // P PM",         
                "~String.Format(" + "{ 0:z zz zzz}" + ",      dt);  //  - 6 -06 -06:00",

            });
            }
            else if (code == "ASPFoundExample" && owner == "Teacher")
            {
                back_end_code = new List<string>(new string[]{
                "int i = 1;",
            "switch (i)",
            "{",
            "~case 1:",
                "~~Console.WriteLine("+"One"+");",
                "~~break;",
                "~case 2:",
                "~~Console.WriteLine("+"Two"+");",
                "~~Console.WriteLine("+"Two"+");",
                "~~break;",
                "~default:",
                "~~Console.WriteLine("+"Other"+");",
                "~~break;",            
            });
            }
            else
            {
                back_end_code = new List<string>();
            }

            int i = 0;
            foreach (string code_line in back_end_code)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }


            DataView codeview = new DataView(codedata);
            return codeview;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CreateCodeSource();
            Code_BackEnd.DataSource = ds;

            /*Some formatting in the codebehind*/


            Code_BackEnd.DataBind();



        }
    }
}